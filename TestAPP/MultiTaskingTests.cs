﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Soneta.Business;
using Soneta.Kadry;
using TestAPP;

[assembly: Worker(typeof(MultiTaskingTests), typeof(Pracownicy))]
namespace TestAPP
{
    public class MultiTaskingTests : ContextBase
    {
        public Log testLog = new Log("Test", true);
        public MultiTaskingTests(Context context) : base(context)
        {

        }
        [Action("MultiTasking", Mode = ActionMode.SingleSession, Target = ActionTarget.ToolbarWithText)]
        public void Action()
        {
            var pracownicy = KadryModule.GetInstance(Context.Session).Pracownicy.Cast<Pracownik>().ToList();
            var pracownicy1 = KadryModule.GetInstance(Context.Session).Pracownicy.Cast<Pracownik>().ToList();

            List<decimal> wyplaty = new List<decimal>();
            List<decimal> wyplaty1 = new List<decimal>();
            pracownicy.ForEach(x => wyplaty.Add(x?.Last?.Etat?.Zaszeregowanie?.Stawka != null ? x.Last.Etat.Zaszeregowanie.Stawka.Value : 0));
            pracownicy1.ForEach(x => wyplaty1.Add(x?.Last?.Etat?.Zaszeregowanie?.Stawka != null ? x.Last.Etat.Zaszeregowanie.Stawka.Value : 0));


            decimal low = 0;
            decimal high = 0;

            Stopwatch sw = new Stopwatch();
            sw.Start();
            var task1 = new Thread(

                () =>
                {
                    for (int i = 0; i < wyplaty.Count(); i++)
                    {
                        var currentWyplata = wyplaty.ElementAt(i);

                        if (low > currentWyplata)
                        {
                            low = currentWyplata;
                        }
                    }
                });
            var task2 = new Thread(

                () =>
                {
                    for (int i = 0; i < wyplaty1.Count(); i++)
                    {
                        var currentWyplata = wyplaty1.ElementAt(i);

                        if (high < currentWyplata)
                        {
                            high = currentWyplata;
                        }
                    }
                });
            task1.Start();
            task2.Start();
            sw.Stop();
            testLog.WriteLine($"Task time elapsed {sw.Elapsed}");
            Pracownik low1 = null;
            Pracownik high1 = null;

            sw.Reset();
            sw.Start();
            for (int i = 0; i < wyplaty1.Count(); i++)
            {
                var currentWyplata = wyplaty1.ElementAt(i);

                if (low > currentWyplata)
                {
                    low = currentWyplata;
                }
            }
            for (int i = 0; i < wyplaty1.Count(); i++)
            {
                var currentWyplata = wyplaty1.ElementAt(i);

                if (high < currentWyplata)
                {
                    high = currentWyplata;
                }
            }
            sw.Stop();
            testLog.WriteLine($"Normal time elapsed {sw.Elapsed}");
        }
    }
}
