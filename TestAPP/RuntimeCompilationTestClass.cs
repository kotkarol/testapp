﻿namespace TestAPP
{
    public class RuntimeCompilationTestClass
    {
        public string slowo { get; set; }
        public int superLiczba { get; set; }
        public RuntimeCompilationTestClass() { }
        public RuntimeCompilationTestClass(string str, int liczba)
        {
            slowo = str;
            superLiczba = liczba;
        }
        public void SayHello()
        {
            string s = "I'm just saying hello!";
        }
    }
}
