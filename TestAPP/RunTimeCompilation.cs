﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.CSharp;
using Microsoft.Win32;
using Soneta.Business;
using Soneta.Core;
using Soneta.Kadry;
using TestAPP;
using TestAPP.Properties;
using System.Configuration;
using System.Net.Mime;

//[assembly: Worker(typeof(RunTimeCompilation), typeof(Pracownik))]
namespace TestAPP
{
    public class RunTimeCompilation
    {

        private string source = Resources.RuntimeCompilationTestClass;
            
        public RunTimeCompilation()
        {
        }

       // [Action("RuntimeCompilation", Mode = ActionMode.SingleSession, Target = ActionTarget.ToolbarWithText)]
        public void Action()
        {
           
            CSharpCodeProvider provider = new CSharpCodeProvider();

            CompilerParameters compilerParams = new CompilerParameters
            {
                GenerateInMemory = true,
                GenerateExecutable = false
            };
            
            
            CompilerResults results = provider.CompileAssemblyFromSource(compilerParams, source);
            
            if (results.Errors.Count != 0)
                throw new Exception("Mission failed!");

            object o = results.CompiledAssembly.CreateInstance("TestAPP.RuntimeCompilationTestClass");
            ConstructorInfo test = o.GetType().GetConstructors().Last();
            object[] args = {"tescior" , 128};
            object createdObject = test.Invoke(args);

            if (createdObject?.GetType().GetProperty("slowo")?.GetValue(createdObject) != null)
            {
                object ultraFinalResultValue = createdObject.GetType().GetProperty("slowo").GetValue(createdObject);
            }
            if (createdObject?.GetType().GetProperty("superLiczba")?.GetValue(createdObject) != null)
            {
                object ultraFinalResultValue = createdObject.GetType().GetProperty("superLiczba").GetValue(createdObject);
            }

            Console.ReadKey();
            var texasdst = AppDomain.CurrentDomain.BaseDirectory;
            MethodInfo mi = o.GetType().GetMethod("SayHello");
            mi.Invoke(o, null);

        }
    }
}
